volatile int CONTADOR = 0; //CONTADOR es una variable global porque se modifica en el loop y en la ISR 
int ANTERIOR = 0;

void setup() {
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(2), sensor, RISING);

}

void loop() {
  if (ANTERIOR != CONTADOR){
    Serial.println(CONTADOR);
    ANTERIOR = CONTADOR;
  }
  
}

void sensor(){
  CONTADOR++;
}
