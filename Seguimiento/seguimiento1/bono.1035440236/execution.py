from Humano import humano  # importamos toda la clase humano e iniciamos.


if __name__=="__main__":
    # instanciar a clase
    edad_elisa=40 # edad de elisa
    nombre="elisa" # nombre
    elisa=humano(edad_elisa,nombre) # recibe la edad de elisa y su nombre
    # llamar atributos.
    print("edad {} tiene {} años".format(elisa.nombre,elisa.edad)) #edad y nombre
    print(elisa.infoHumano())  # imprime el nombre y la edad
    #print("name:{}".format(__name__))
    print("viewedad {} ".format(elisa.viewedad())) # imprime la edad
    edad_pedro=30 # edad de pedro
    nombre_pedro="pedro" # nombre de pedro
    pedro=humano(edad_pedro,nombre_pedro) # 
    print(pedro.infoHumano()) # imprime nombre de pedro y edad
    print("viewedad {}".format(pedro.viewedad())) # edad de pedro


