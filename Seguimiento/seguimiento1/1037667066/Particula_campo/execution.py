from Particulacampo import movimiento

"""
NOTA 
4.7

- El código no funciona en los limites por ejemplo en B=0, o decirle al usuario que 
valores poner

"""

if __name__=="__main__":

    m=9.109*10**(-31) #masa de la particula en kg
    q=-1.602*10**(-19) #carga de la particula en C
    E=18.6*(1.602*10**(-19)) #Energia cinetica de la particula en Joules
    B=0#6e-4 #campo magnetico en T
    ang=90 #angulo que forma la velocidad con el campo magnetico en grados
    ciclos=10  #Numero de vueltas

    mov=movimiento(m,q,E,B,ang,ciclos)
    print("Velocidad Inicial de la particula: {} m/s".format(mov.v0()))
    print("Radio de la helice: {} m".format(mov.R()))
    print("Periodo de la helice: {} s".format(mov.T()))
    print("Frecuencia angular de ciclotron: {} rad/s".format(mov.w))

    graf=mov.grafica()