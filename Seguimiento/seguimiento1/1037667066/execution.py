from Tiroparabolic import tiro
from Tiroparabolic import tiro2

if __name__=="__main__":
    v0=30  #velocidad inicial
    a=90  #angulo inicial 
    y0=20  #altura inicial
    x0=0  #posicion en x inicial
    g=9.8 #gravedad debe ser positiva
    air=5 #resistencia del aire
    graf1='Tiro parabolicon sin aire'
    graf2='Tiro parabolico con aire'

    ti=tiro(v0,a,y0,x0,g,graf1)
    ti2=tiro2(v0,a,y0,x0,g,graf2,air)

    #llamar metodos
    print('Velocidad inicial en x: {} m, Velocidad inicial en y: {} m '.format(ti.v0x(),ti.v0y()))
    print('Tiempo de vuelo: {} s'.format(ti.tv()))
    print('Tiempo de altura maxima: {} s'.format(ti.tm()))
    print('Altura maxima: {} m'.format(ti.ym()))
    print('Distancia en x maxima: {} m'.format(ti.xm()))

    graf=ti.graf()

    print('Datos con aire:')
    print('Velocidad inicial en x: {} m, Velocidad inicial en y: {} m '.format(ti2.v0x(),ti2.v0y()))
    print('Tiempo de vuelo: {} s'.format(ti2.tv()))
    print('Tiempo de altura maxima: {} s'.format(ti2.tm()))
    print('Altura maxima: {} m'.format(ti2.ym()))
    print('Distancia en x maxima: {} m'.format(ti2.xm()))

    graf=ti2.graf()