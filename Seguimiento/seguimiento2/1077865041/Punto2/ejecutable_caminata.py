import caminata as cm

#buen trabajo, el N de los pasos va dentro de _main_
#4.8

N = 100 #Número de pasos

if __name__=='__main__':
    #Apliquemos la clase camino
    a=cm.camino(N)
    #Se generan las posiciones
    a.posiciones()
    #Se grafican las posiciones
    a.grafica()
    