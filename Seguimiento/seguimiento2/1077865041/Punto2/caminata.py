import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#Clase para la caminata aleatoria
class camino:
    
    #Método constructor

    def __init__(self,N):
        self.N = N
        self.N2 = self.N

    #Definamos el método para las posiciones en x,y,z
    def posiciones(self):
        #Se definen las posiciones iniciales
        self.x = [0]
        self.y = [0] 
        self.z = [0]
        #Se genera el camino aleatorio con 6 posibilidades de igual probabilidad
        while self.N > 0 :
                #Se genera un número aleatorio entre 0 y 150
                number = np.random.randint(0,150)
                #Para moverse en el eje y
                if number <= 25:
                    self.y.append(self.y[0]+1)
                    self.x.append(0)
                    self.z.append(0)
                elif 25 < number <= 50:
                    self.y.append(self.y[0]-1)
                    self.x.append(0)
                    self.z.append(0)
                    #Para moverse en el eje x
                elif 50 < number <= 75:
                    self.x.append(self.x[0]+1)
                    self.y.append(0)
                    self.z.append(0)
                elif 75 < number <= 100:
                    self.x.append(self.x[0]-1)
                    self.y.append(0)
                    self.z.append(0)
                    #Para moverse en el eje z
                elif 10 < number <= 125:
                    self.x.append(0)
                    self.y.append(0)
                    self.z.append(self.z[0]+1)
                elif 75 < number <= 100:
                    self.x.append(0)
                    self.y.append(0)
                    self.z.append(self.z[0]-1)

                
                self.N = self.N-1
    #Se grafican las posiciones
    def grafica(self):

        # Se genera el acumulado y se grafica la posición final
        newposx = np.cumsum(self.x)
        newposy = np.cumsum(self.y)
        newposz = np.cumsum(self.z)
        #Grafiqemos en tres dimensiones
        fig = plt.figure(figsize = (10,10))
        ax = fig.add_subplot(111, projection='3d')
        plt.title('Camino del borracho con {} pasos'.format(self.N2))
        ax.plot(newposx,newposy,newposz,'-o')
        ax.plot(newposx[0],newposy[0],newposz[0],'o',color ='orange',label = 'Posición inicial')
        ax.plot(newposx[-1],newposy[-1],newposz[-1],'o', color = 'red',label = 'Posición final')
        ax.set_xlabel('x',fontsize = 15)
        ax.set_ylabel('y',fontsize = 15)
        ax.set_zlabel('z',fontsize = 15)
        plt.legend()
        plt.savefig('camino3d.png')
        plt.show()
