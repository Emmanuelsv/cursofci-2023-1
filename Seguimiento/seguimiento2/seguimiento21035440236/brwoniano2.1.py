from browniano import browniano
import numpy as np
import matplotlib.pyplot as plt
if __name__=="__main__":
    N=1000
    dt=0.01
    media= 0 # centrada en el origen
    desviacion=1  # esta normalizada media 0 y sigma 1 esto con el proposito se simplificar los calculos.
    execution=browniano(N,dt,media,desviacion)
    print("los valores aleatorios posibles de la particula son",execution.movimiento())
    print("la grafica es",execution.grafica())
