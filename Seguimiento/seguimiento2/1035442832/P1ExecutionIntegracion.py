# Punto 1: Integración con Monte Carlo. Ejecución.

import P1ClassIntegracion
import numpy as np
#BUEN TRABAJO
#5.0

if __name__=='__main__': # Llamo la ejecución.
  # Defino atributos.
  a = 0 # Límite inferior de la integral.
  b = np.pi # Límite superior de la integral.
  N = 1e3 # Número de iteraciones.
  f = lambda x: ((x**2)*np.cos(x)) # Función a integrar.

  sol = solIntegral(a, b, N, f) # Instanciando la clase.

  sol.grafComp() # Gráfica comparativa. Tarda como 8 min.

  # Imprimiendo los valores por cada método.
  print("La integral de la función dada en el intervalo es, por el método exacto,\
  aproximadamente: ", np.round(sol.metExacto(),5))
  
  print("La integral de la función dada en el intervalo es, por el método de\
  Monte Carlo, aproximadamente: ", np.round(sol.monteCarlo(),5))