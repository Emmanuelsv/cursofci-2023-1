from Integracion import integral_MC2
from Integracion import Integrales_SP
import numpy as np
from sympy import *
# Muy buen trabajo
#5.0

if __name__=="__main__":

    n,a,b = 10000000,0,np.pi
    x = np.random.uniform(a,b,n)


    f = lambda x: x**2* np.cos(x)
    val_x = np.linspace(0,np.pi,1000)
    f_v = f(val_x)
    f_min = np.min(f_v)
    f_max = np.max(f_v)

    y = np.random.uniform(f_min,f_max,n)

    I1 = integral_MC2(n,a,b,x,y,f)



    I1.graficar()
    print(I1.Integral())

    # Clase integrales en sympy

    x = symbols('x')

    x1 = 0

    x2 = np.pi

    g = x**2 * cos(x)

    I2 = Integrales_SP(g, x1, x2 ,x)

    print(I2.IntegralS())



