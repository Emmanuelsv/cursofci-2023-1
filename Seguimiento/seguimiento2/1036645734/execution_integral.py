
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from seguim_2 import seguimiento_2
from scipy.integrate import quad

# Buen trbajo
#5.0

if __name__=="__main__":
    #Parámetros del problema
    n = 1000000
    a = 0
    b = np.pi

    def f(x):
        return x**2 * np.cos(x)
    
    ymax = max(f(np.linspace(a,b,n)))
    ymin = min(f(np.linspace(a,b,n)))

    x=np.random.uniform(0, np.pi, size=(1, n))
    y=np.random.uniform(ymin, ymax, size=(1, n))
    
    #instanciar clase (hacer un objeto)
    seguimiento=seguimiento_2(n,x,y) 

    #Se definen las máscaras
    interior1 = ((y <= seguimiento.f()) & (y>0)).sum()
    interior2 = ((y>=seguimiento.f()) & (y<0)).sum()*-1
    interior = interior1+interior2
    #exterior = np.invert(interior)
    
    seguim_2 = interior.sum()/n #probabilidad
    I1 = (b-a)*(ymax-ymin)*seguim_2


def integrand(xs):
    return (xs**2)*np.cos(xs)

xs = np.arange(0,np.pi,0.001)
I = quad(integrand,0,np.pi)
print(I)
print(I1)
print(interior)

#Graficación
#plt.plot(x[exterior], y[exterior], 'bo')
plt.plot(x[interior], y[interior], 'co')
plt.plot(xs,integrand(xs), 'r--',label='f(x)')
plt.xlim(0,3.2)
plt.ylim(-10,1)
plt.legend()
plt.show()
plt.savefig('seguim_2_p1.png')

