import numpy as np
import matplotlib.pyplot as plt
import math

class ColisionadorCircular:
    def __init__(self, R, r, n_values):
        self.R = R
        self.r = r
        self.n_values = n_values
        self.probabilidad_de_col = []

    def simular_colisiones(self):
        for n in self.n_values:
            theta1 = np.random.uniform(0, 2*np.pi, size=int(n))  # Ángulo de partículas en cara A
            theta2 = np.random.uniform(0, 2*np.pi, size=int(n))  # Ángulo de partículas en cara B

            x1 = (self.R - self.r) * np.cos(theta1)  # Coordenada x de partículas en cara A
            y1 = (self.R - self.r) * np.sin(theta1)  # Coordenada y de partículas en cara A

            x2 = -(self.R - self.r) * np.cos(theta2)  # Coordenada x de partículas en cara B
            y2 = -(self.R - self.r) * np.sin(theta2)  # Coordenada y de partículas en cara B

            exp_v = ((x1**2 + y1**2) < (self.R-self.r)**2) & ((x2**2 + y2**2) < (self.R-self.r)**2)

            if np.any(exp_v):  # Verificar si hay partículas que cumplan con la condición
                colisiones = np.vectorize(lambda x1, y1, x2, y2: True if math.dist([x1, y1], [x2, y2]) <= 2*self.r else False)\
                    (x1[exp_v], y1[exp_v], x2[exp_v], y2[exp_v])  # Distancia euclidiana entre partículas < 2R
                self.probabilidad_de_col.append(colisiones.sum() / exp_v.sum())
            else:
                self.probabilidad_de_col.append(0)  # Establecer probabilidad de colisión en cero



    def graficar_probabilidad_de_colision(self):
        self.simular_colisiones()
        plt.plot(self.n_values, self.probabilidad_de_col, '.-')
        plt.xlabel('Cantidad de iteraciones (n)')
        plt.ylabel('Probabilidad de colisión')
        plt.title('Probabilidad de colisión vs Cantidad de iteraciones')
        plt.grid(True)
        plt.show()
