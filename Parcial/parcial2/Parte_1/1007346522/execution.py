from Dados import*
from Colisionador import *

if __name__=='__main__':
    print(f'Your name is {__name__}')

    # ---- Punto 1 ----
    N_Dados = 3

    Dados1 = Dados(N_Dados)   

    #Dados1.Graf(Tipo = 's') # Tipo '1a1' o 's' Simultaneo
    ## Vease Plot_Eventos_Dados.png

    # --- Punto 2 ---

    RadioColsionador = 10 
    RadioParticula = 0.1

    Colision = Colisionador(RadioColsionador,RadioParticula)

    print(Colision.Probability_colision())


