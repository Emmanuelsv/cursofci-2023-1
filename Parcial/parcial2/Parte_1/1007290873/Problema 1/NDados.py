#La simulación esta bien, los histogramas NO
#Falta la segunda parte
#2.0
import numpy as np
import matplotlib.pyplot as plt


n = 10000
n_array = np.linspace(1, n, 1000)

# Número de dados N = 2, 3, 4

possible_outcomes_2 = np.arange(2, 13, 1)
possible_outcomes_3 = np.arange(3, 19, 1)
possible_outcomes_4 = np.arange(4, 25, 1)

np.random.seed(1)

def prob_of_sum(n, possible_outcomes):

    outcome_prob_list = []

    for outcome in possible_outcomes:

        die_number = possible_outcomes[0]

        random_results = []

        for i in range(die_number):

            random_results.append(np.random.randint(1, 6, int(n)))

        outcome_mask = (sum(random_results) == outcome)

        prob_outcome = outcome_mask.sum()/n
        outcome_prob_list.append(prob_outcome)

    return outcome_prob_list

results_2 = prob_of_sum(n, possible_outcomes_2)
results_3 = prob_of_sum(n, possible_outcomes_3)
results_4 = prob_of_sum(n, possible_outcomes_4)

plt.figure()
plt.bar(possible_outcomes_2, results_2, align='center') # A bar chart
plt.xlabel('Posible suma')
plt.ylabel('Probabilidad')
plt.title('2 Dados')
plt.savefig("2DadosHist.png")

plt.figure()
plt.bar(possible_outcomes_3, results_3, align='center') # A bar chart
plt.xlabel('Posible suma')
plt.ylabel('Probabilidad')
plt.title('3 Dados')
plt.savefig("3DadosHist.png")

plt.figure()
plt.bar(possible_outcomes_4, results_4, align='center') # A bar chart
plt.xlabel('Posible suma')
plt.ylabel('Probabilidad')
plt.title('4 Dados')
plt.savefig("4DadosHist.png")


