import numpy as np
import matplotlib.pyplot as plt
import sympy as sy

class penduloSimple():

    def __init__(self,a,b,x0,y0,n,sq_gl):
        self.a=a
        self.b=b
        self.x0=x0
        self.y0=y0
        self.h=(b-a)/n
        self.n=n
        self.w0=sq_gl
        self.T=np.arange(self.a,self.b+self.h,self.h)
    
    def arrayEuler(self):
        SOL = np.zeros((self.n+1, 2))

        # Cond. iniciales 
        SOL[0,0] = self.x0        # theta
        SOL[0,1] = self.y0        # omega

        # Metodo de Euler
        for i in range(self.n):
            SOL[i+1,0] = SOL[i,0] + self.h*SOL[i,1]             # solución theta
            #SOL[i+1,1] = SOL[i,1] - self.h*self.w0**2*np.sin(SOL[i,0])         # solución omega sin aproximación
            SOL[i+1,1] = SOL[i,1] - self.h*self.w0**2*(SOL[i+1,0])            # solución omega con aproximación
        return SOL[:,0], SOL[:,1]                                           # de pequenos angulos
    
    def PlotPos(self):
        font = {'weight' : 'bold', 'size'   : 10}
        plt.rc('font', **font)
        plt.title("Posición angular")
        plt.plot(self.T,self.arrayEuler()[0],label="solución con Euler")
        plt.plot(self.T,self.SolAna()[0],label="solución análitica")
        plt.xlabel("Tiempo [s]")
        plt.ylabel("Desplazamiento angular [rad]")
        plt.legend()
        plt.grid()
        plt.savefig("Posicion_angular.png")
        print("Se ha guardado la comparación al desplazamiento angular como Posicion_angular.png")
        plt.close()
 
    def PlotVel(self):
        font = {'weight' : 'bold', 'size'   : 10}
        plt.rc('font', **font)
        plt.title("Velocidad angular")
        plt.plot(self.T,self.arrayEuler()[1],label="solución con Euler")
        plt.plot(self.T,self.SolAna()[1],label="solución análitica")
        plt.xlabel("Tiempo [s]")
        plt.ylabel("Velocidad angular [rad]")
        plt.legend()
        plt.grid()
        plt.savefig("Velocidad_angular.png")
        print("Se ha guardado la comparación de la velocidad angular como Velocidad_angular.png")

    def SolAna(self):
        x=sy.Symbol('x')
        y=sy.Function("y")

        sol_theta = sy.dsolve(y(x).diff(x,2) + self.w0**2*y(x))         # solucionamos la ED y'=f(x,y)

        #se reemplazan las condiciones iniciales en la solución de la ED y se hallan sus valores
        ics=[sy.Eq(sol_theta.args[1].subs(x, 0), self.x0), sy.Eq(sol_theta.args[1].diff(x).subs(x, 0), self.y0)]
        solved_ics = sy.solve(ics)
        Sol_theta = sol_theta.subs(solved_ics)
        Sol_omega=Sol_theta.rhs.diff(x)

        #se hacen las soluciones funciones de numpy
        Sol_theta_np=sy.lambdify(x,Sol_theta.rhs)
        Sol_omega_np=sy.lambdify(x,Sol_omega)

        return Sol_theta_np(self.T), Sol_omega_np(self.T)

