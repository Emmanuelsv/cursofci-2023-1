
# Importando clases y librerías.
from ClassEDO_P3_E1_FCI import solNumerica, penduloSimple
import sympy as sym

if __name__ == '__main__': # Inicializando ejecución.

    # Primera parte del punto, comparación métodos.

    a = 0 # Inicio del intervalo X.
    b = 1 # Final del intervalo X.
    n = 1000 # Número de particiones.
    y0 = -1 # Condición inicial.
    #point = 1.8 # Punto para evaluar solución.
    f =  lambda x,y: sym.exp(-x) # Función a resolver.
    
    # Instanciar/llamar clase.
    soledo = solNumerica(a, b, n, y0, f)

    soledo.figCompResEDO('Comparación solución numérica y solución exacta', "SolNumVsSolExac.png") # Llamando método de clase.
    
    # Segunda parte del punto, péndulo simple.
    
    # Configurando los parámetros.
    ap = 0 # Tiempo inicial.
    bp = 5 # Tiempo final.
    np = 0.1 # Paso para arreglo.
    wn = 4 # Frecuencia Natural (raiz de g/l).
    theta0 = 1 # Condición inicial theta.
    s0 = 0 # Condición inicial nueva variable.

    edo1 = lambda s1: s1 # EDO's con nueva variable.
    edo2 = lambda s0: -(wn**2)*s0  

    # Instanciar clase.
    sol2 = penduloSimple(ap, bp, np, s0, edo1, theta0, wn, edo2)
    
    sol2.DesplazamientoAngular('Péndulo Simple', "PendSimpleSolNumExac.png") # Llamando método de clase.
