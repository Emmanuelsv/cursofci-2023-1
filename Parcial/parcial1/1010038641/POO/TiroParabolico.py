import numpy as np
from matplotlib import pyplot as plt


class TiroParabolico():

    #primero definimos el constructor
    def __init__(self, x0, y0, v0, theta, ay, ax):
        print('inicializando clase de movimiento parabólico')

    # inicializamos los atributos

        self.x0 = x0 # posición inicial en x
        self.y0 = y0 # posición inicial en y
        self.v0 = v0 # valor de la velocidad inicial 
        self.theta = np.deg2rad(theta)
        self.ay = ay
        self.ax = ax

    # Ahora nuestros métodos
      
    # velocidad inicial en x
    def v0_x(self):
        return self.v0*np.round(np.cos(self.theta), 2)
    
    # velocidad inicial en y
    def v0_y(self):
        return self.v0*np.round(np.sin(self.theta), 2)

    # tiempo de vuelo
    def tiempo_vuelo(self):
        if self.ay == 0:
            raise Warning('la gravedad no puede ser 0')
        else:
            tv = (self.v0_y() + np.sqrt(self.v0_y()**2 +  2 * self.ay * self.y0))/self.ay
            return tv

         #else:
        #    raise Warning ('Error, la aceleración no puede ser 0')


    def altura(self):
        return self.y0 + self.v0_y() * self.tiempo_vuelo()/2 - 1/2 * self.ay * (self.tiempo_vuelo()/2) ** 2

    def alcance_x(self):
        return self.x0 + self.v0_x() * self.tiempo_vuelo() - 1 / 2 * self.ax * self.tiempo_vuelo() ** 2

    # array de tiempo
    def arr_time(self):
        return np.arange(0, self.tiempo_vuelo(), 0.01)


    # posición en x
    def x(self):
        return [self.x0 + i * self.v0_x() - 1 / 2 * self.ax * i ** 2 for i in self.arr_time()]

    # posición en y
    def y(self):
        return [self.y0 + i * self.v0_y() - 1 / 2 * self.ay * i ** 2 for i in self.arr_time()]


    # graficación
    def graficar(self):
        f, ax = plt.subplots()
        plt.title('Movimiento Parabólico')
        ax.plot(self.x(), self.y())
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.grid()
        plt.show()
        return


'''
Para la parte de herencia, haremos una clase que calcule el ángulo de lanzamiento para alcanzar una distancia d dada una velocidad inicial"
Referencia: https://en.wikipedia.org/wiki/Projectile_motion
'''

class PooTirop(TiroParabolico):
    def __init__(self, x0, y0, v0, theta, d, ay, ax=0):
        super().__init__(x0, y0, v0, theta, ay, ax=0)
        self.d = d

    def angulo(self):
        return np.round(np.rad2deg(0.5 * np.arccos(self.ay * self.d / self.v0**2)))