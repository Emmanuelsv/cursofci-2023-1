from EcuacionesDiferencialesSolucionador import ecuacionesDiferencialesSolucionador
import sympy as sym

if __name__ == "__main__":

    # Definimos los valores  que se sugieren para los parámetros de entrada de la clase

    x0 =  0
    y0 = -1
    x_inicial = 0
    x_final = 1 
    n = 100
    ODE_function = lambda x, y: sym.exp(-x)
    # Se puede ingresar un valor de x particular en donde llevar a cabo la evaluación:
    # x_evaluacion = 

    # Creamos el objeto de la clase ecuacionesDiferencialesSolucionador correspondiente a estos valores
    
    solucion_problema_EDO = ecuacionesDiferencialesSolucionador(x0, y0, x_inicial, x_final, n, ODE_function)

    print('Solución de la ecuación diferencial con el método de Euler: y = {} en x = {}'.format(solucion_problema_EDO.ResolverEuler()[0][-1], solucion_problema_EDO.DarArregloX()[-1]))
    print('Solución de la ecuación diferencial con método analítico: y = {}  en x = {}'.format(solucion_problema_EDO.SolucionAnalitica()[0][-1], solucion_problema_EDO.DarArregloX()[-1]))
    solucion_problema_EDO.GraficaComparacion()