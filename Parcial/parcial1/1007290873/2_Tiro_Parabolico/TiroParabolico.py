import numpy as np
import matplotlib.pyplot as plt
class tiroParabolico(): 

    """
    Clase que permite describir el problema del tiro parabólico, con aceleración en los ejes x y y
    """

    # Constructor

    def __init__(self, v_0, alpha, x_0, y_0, a_y, a_x):

        """
        Parámetros de entrada para instanciar la clase              
                                                                        | Unidades
        v_0: magnitud de la velocidad inicial del proyectil             |  [m/s] 
        alpha: ángulo de la velocidad inicial con respecto al eje x     | [grados]
        x_0: posición inicial en x                                      |   [m]
        y_0: posición inicial en y                                      |   [m]
        a_y: aceleración constante en y                                 |  [m/s^2]
        a_x: aceleración constante en x                                 |  [m/s^2]
        """

        # Definición de atributos

        # Atributos correspondientes a los parametros de entrada
        self.vel_inicial = v_0
        self.angulo_grados = alpha
        self.angulo = np.deg2rad(alpha) # El ángulo se convierte a radianes
        self.pos_inicial_x = x_0
        self.pos_inicial_y = y_0
        self.acl_y = a_y
        self.acl_x = a_x

        # Atributos correpondientes a las dos componentes de la velocidad inicial
        # obtenidas a partir de los atributos de magnitud de la velocidad inicial y el ángulo
        self.vel_inicial_x = v_0 * np.cos(self.angulo)
        self.vel_inicial_y = v_0 * np.sin(self.angulo)

        
    # Definición de métodos

    def PosicionEnX(self, t):
        return self.pos_inicial_x + self.vel_inicial_x * t + 0.5 * self.acl_x * t**2

    def PosicionEnY(self, t):
        return self.pos_inicial_y + self.vel_inicial_y * t + 0.5 * self.acl_y * t**2

    def VelocidadEnX(self, t):
        return self.vel_inicial_x + self.acl_x * t

    def VelocidadEnY(self, t):
        return self.vel_inicial_y + self.acl_y * t

    def TiempoMax(self): 
        
        # Tiempo que corresponde a la altura máxima

        acl = float(self.acl_y)

        if acl == 0.0:
            raise ValueError("La aceleración debe ser distinta de 0")
        elif acl > 0:
            raise ValueError("No hay t máximo si la aceleración > 0")
        else:
            return -self.vel_inicial_y / acl
        

    def PosicionXEnAlturaMax(self): 
        # Posición en x cuando se da la altura máxima
        return self.PosicionEnX(self.TiempoMax())

    def AlturaMax(self): 
        # Altura máxima
        return self.PosicionEnY(self.TiempoMax())

    def TiempoArbitrario(self):
        # Tiempo arbitrario entre 5s y 30s, para los casos en que a > 0
        # Se usa en el método TiempoVuelo
        return np.random.randint(5, 30)
    
    def TiempoVuelo(self):
        # Tiempo que tarda el proyectil en llegar a la posición y = 0
        acl = float(self.acl_y)

        if acl == 0.0:
            raise ValueError("La aceleración debe ser distinta de 0") 
        elif acl > 0:
            # En este caso no hay un tiempo de vuelo, aún así se regresa tiempo arbitrario para poder graficar
            return self.TiempoArbitrario()
        else:
            return ( - self.vel_inicial_y - np.sqrt( self.vel_inicial_y ** 2 - (2 * self.pos_inicial_y * acl) ) ) / acl

    def AlcanceMax(self):
        # Posición en x cuando el proyectil llega a y = 0
        return self.PosicionEnX(self.TiempoVuelo())
        

    def GraficaMovimiento(self):

        # Array de tiempos:
        tiempos = np.linspace(0, self.TiempoVuelo(), 100)

        # Arrays de las posiciones
        x_posiciones = self.PosicionEnX(tiempos)
        y_posiciones = self.PosicionEnY(tiempos)

        # Gráfica:
        fig, ax = plt.subplots(figsize=(18,15))
        ax.plot(x_posiciones, y_posiciones, color='red', label = "Trayectoria del proyectil")
        ax.set_title("Gráfica del movimiento parabólico para las condiciones dadas", fontsize=20)
        ax.set_xlabel("$x(t)$ (m)",fontsize=20)
        ax.set_ylabel("$y(t)$ (m)",fontsize=20)
        ax.legend(fontsize = 16) 
        ax.grid()
        fig.savefig("TiroParabolico.png")
        

class tiroParabolicoPlanoInclinado(tiroParabolico):
    
    """
    Clase que permite describir el problema del tiro parabólico, con aceleración en los ejes x y y,
    En esta clase estamos considerando el caso en el cual se tiene un plano inclinado con un ángulo especificado
    """

    # Constructor 

    def __init__(self, v_0, alpha, x_0, y_0, a_y, a_x, theta):


        """
        Parámetros de entrada para instanciar la clase              
                                                                                           | Unidades
        v_0: magnitud de la velocidad inicial del proyectil                                |  [m/s] 
        alpha: ángulo de la velocidad inicial con respecto al eje x del plano inclinado    | [grados]
        x_0: posición inicial en x                                                         |   [m]
        y_0: posición inicial en y                                                         |   [m]
        a_y: aceleración constante en y                                                    |  [m/s^2]
        a_x: aceleración constante en x                                                    |  [m/s^2]
        theta: ángulo de inclinación del plano (debe ser menor a pi/2)                     |  [grados]
        """

        # Definición de atributos

        # Se heredan los atributos de la clase tiroParabolico
        super().__init__(v_0, alpha, x_0, y_0, a_y, a_x)

        self.angulo_inclinado = np.deg2rad(theta)

        # Modificamos atributos de la clase tiroParabolico, para resolver el problema
        # desde una rotación del eje de coordenadas
        self.pos_inicial_x = np.cos(self.angulo_inclinado) * x_0 + np.sin(self.angulo_inclinado) * y_0
        self.pos_inicial_y = - np.sin(self.angulo_inclinado) * x_0 + np.cos(self.angulo_inclinado) * y_0

        self.acl_x = np.cos(self.angulo_inclinado) * a_x - np.sin(self.angulo_inclinado) * a_y
        self.acl_y =  np.sin(self.angulo_inclinado) * a_x + np.cos(self.angulo_inclinado) * a_y
    
    """
    Los métodos que se heredan de por ejemplo posición o velocidad dan valores con respecto unos ejes x y y
    que son rotados, es decir tales que el eje x coincide con el plano inclinado 
    """

    # Definición de métodos modificados o nuevos con respecto a tiroParabolico

    def rotarX(self, x, y):
        # Para volver del sistema rotado del plano, al sistema desde donde se va a graficar una posición en x
        return np.cos(self.angulo_inclinado) * x - np.sin(self.angulo_inclinado) * y

    def rotarY(self, x, y):
        # Para volver del sistema rotado del plano, al sistema desde donde se va a graficar una posición en y
        return np.sin(self.angulo_inclinado) * x + np.cos(self.angulo_inclinado) * y

    def GraficaMovimiento(self):

        recta_plano_inclinado = lambda x, theta: np.tan(theta) * x

        # Array de tiempos:
        
        tiempos = np.linspace(0, self.TiempoVuelo(), 100)

        # Arrays de las posiciones
        x_posiciones = self.rotarX(self.PosicionEnX(tiempos), self.PosicionEnY(tiempos))
        y_posiciones = self.rotarY(self.PosicionEnX(tiempos), self.PosicionEnY(tiempos))

        acl = float(self.acl_y)

        if acl > 0:
            x_plano = np.linspace(-5, 5, 100)
            y_plano = np.linspace(recta_plano_inclinado(-5, self.angulo_inclinado), recta_plano_inclinado(5, self.angulo_inclinado), 100)
        elif acl < 0:
            x_plano = np.linspace(0, x_posiciones[-1], 100)
            y_plano = np.linspace(0, y_posiciones[-1], 100)

        # Gráfica:
        fig, ax = plt.subplots(figsize=(18,15))
        ax.plot(x_posiciones, y_posiciones, color='red', label = "Trayectoria del proyectil")
        ax.plot(x_plano, y_plano, color = "black", linewidth = 5, label = "Plano inclinado")
        ax.set_title("Gráfica del movimiento parabólico con un plano inclinado para las condiciones dadas", fontsize=20)
        ax.set_xlabel("$x(t)$ (m)",fontsize=20)
        ax.set_ylabel("$y(t)$ (m)",fontsize=20)
        ax.legend(fontsize = 16) 
        ax.grid()
        fig.savefig("TiroParabolicoPlanoInclinado.png")