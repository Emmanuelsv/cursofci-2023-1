import numpy as np
import  matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits import mplot3d
class pendulo ():
    def __init__(self,m,M,R,v_bloque,g,R1,M1,m1):   # definimos los atributos
        self.m=m
        self.M=M
        self.R=R
        self.R1=R1
        self.M1=M1
        self.m1=m1
         # velocidad de la bala
        self.v_bloque=v_bloque # velocidad del bloque
        self.g=g
    def velocidad_bola (self):  # este metodo calcula la velocidad inicial de la bola
        velocidad_bola = ((self.M+self.m)/self.m)*self.v_bloque # calculo la velocidad de la bola
        return velocidad_bola

    def altura(self):  # calcula la altur total
        h=(1/(2*self.g))*(( (self.m/(self.m+self.M))      )*(self.velocidad_bola()) )**2# calcylo la integral

        return h # se calcula la altura despues del choque
    def angulo (self):   # calcula la desviacion del angulo
        angle=(np.arccos(1- (  self.v_bloque**2/(2*self.g*self.R)  )))*180/np.pi
        return angle   # calculo el angulo
    def punto_alto_trayectoria(self):  # calcula velocidad minima de la bola.
        vc=np.sqrt(self.R1*self.g)
        a=(1/2)*vc**2+self.g*2*self.R1
        vo=np.sqrt(2*a)
        u=((self.m1+self.M1)*vo)/self.m1
        
        return u
    # Ahora tenemos que considerar el movimiento armonico simple para un pendulo despues del choque

    def tiempo(self):
        tiempo=np.linspace(0,100) 
        return tiempo
    def angle(self):
        thetha=2.00713*np.sin(31.3*self.tiempo())  
        return thetha  
    def grafica1(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.tiempo(), self.angle(),color="green",lw=3)
        plt.xlabel("posicion t")
        plt.ylabel("posicion angle")
        plt.grid()
        plt.savefig("pendulo_balistico.png")    


      
        
# creamos el polimorfismo

    

class pendulo2(pendulo):   # hacemos una herencia.
    def __init__(self, m, M, R, v_bloque, g,R1,M1,m1):
        super().__init__(m, M, R, v_bloque, g,R1,M1,m1 )
    def velocidad_minima(self): # velocidad minima de la bola para que de una vuelta
        velocidad_bala_minima=(4*self.M*np.sqrt(self.g*self.R))/self.m
        return velocidad_bala_minima # velocidad minima de la bola.
    # definamos la velocidad minima que puede dar la bola en una vuelta.

class pendulo3():
    def __init__(self,m,M,R,v_bloque,g,R1,M1,m1):   # definimos los atributos
        self.m=m
        self.M=M
        self.R=R
        self.R1=R1
        self.M1=M1
        self.g=9.8
        self.m1=m1
    def punto_alto_trayectoria(self):  # calcula velocidad minima de la bola. Aplicamos el polimorfismo
        vc=np.sqrt(self.R1*self.g)
        a=(1/2)*vc**2+self.g*2*self.R1
        vo=np.sqrt(2*a)
        u=((self.m1+self.M1)*vo)/self.m1
        return u
    # creamos el polimorfismo de manera que hacemos otra clase con otro metodo la cual se llama
    # la misma manera pero su forma cambia pero actua con sus mismos metodos.
    


    
    



    




        

    


